# Angular application template

## Content

1. Project generated  with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1
1. hexagon-js added (dark style)
1. phantomjs and html reporter added (tests)
1. Angular Font Awesome added (icons)

## Frontend
* [README](frontend/README.md)
